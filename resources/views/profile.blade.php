@extends('layouts.main',['title'=>'Profile'])
@section('css')
    <link rel="stylesheet" href="{{ asset('packages/datatables/dataTables.bootstrap4.min.css') }}">
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Profile</li>
    </ol>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('update.profile',$user->id) }}" method="post">
                @method('put')
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="ubah_nama" name="ubah_nama" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="ubah_email" name="ubah_email" value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label for="telepon">Telepon</label>
                            <input type="text" class="form-control" id="ubah_telepon" name="ubah_telepon" value="{{ $user->employee->telepon }}">
                        </div>
                        <input type="submit" class="btn btn-primary" value="Ubah">
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="jenisKelamin">Jenis Kelamin</label>
                            <input type="text" class="form-control" id="ubah_jenis_kelamin" name="ubah_jenis_kelamin" value="{{ $user->employee->jenis_kelamin }}">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea name="ubah_alamat" class="form-control" id="ubah_alamat" name="ubah_alamat" rows="4">{{ $user->employee->alamat }}</textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('packages/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('packages/datatables/dataTables.bootstrap4.min.js') }}"></script>
@endsection

