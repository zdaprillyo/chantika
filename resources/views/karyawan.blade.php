@extends('layouts.main',['title'=>'Karyawan'])
@section('css')
    <link rel="stylesheet" href="{{ asset('packages/datatables/dataTables.bootstrap4.min.css') }}">
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
    </ol>
@endsection
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title ">Daftar Karyawan</h4>
            <button class="btn btn-primary" data-toggle="modal" data-target="#tambah-karyawan">Tambah</button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table" id="table-karyawan">
                <thead class=" text-primary">
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Email</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                </thead>
                <tbody>
                    @forelse ($users as $user)
                        <tr>
                            <td>{{ ucfirst($user->name) }}</td>
                            <td>{{ ucfirst($user->employee->jenis_kelamin) }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->employee->telepon }}</td>
                            <td>{{ $user->employee->alamat }}</td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    <x-modal id="tambah-karyawan" title="Tambah Karyawan">
        <form action="{{ route('karyawan.store') }}" method="post">
            {{ csrf_field() }}
            <x-input label="Nama" type="text" name="tambah_nama" required="true" placeholder="Masukkan nama karyawan"/>
            <x-select-sex id="tambah_jenis_kelamin" name="tambah_jenis_kelamin"  required="true"/>
            <x-input label="Alamat" type="text" name="tambah_alamat" required="true" placeholder="Masukkan alamat karyawan"/>
            <x-input label="Email" type="email" name="tambah_email" placeholder="Masukkan email karyawan jika ada"/>
            <x-input label="Telepon" type="text" name="tambah_telepon" required="true" placeholder="Masukkan nomor telepon karyawan"/>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-success" value="Simpan" />
            </div>
        </form>
    </x-modal>
    <x-modal id="hapus-karyawan" title="Hapus Karyawan">
        <form action="" method="delete">
            @csrf
            <h5>Anda yakin akan menghapus kompensasi ini?</h5>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-danger" value="Hapus" />
            </div>
        </form>
    </x-modal>
@endsection
@section('js')
    <script src="{{ asset('packages/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('packages/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#table-karyawan').DataTable()
        })
    </script>
@endsection

