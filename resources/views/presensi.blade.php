@extends('layouts.main',['title'=>'Presensi'])
@section('css')
    <link rel="stylesheet" href="{{ asset('packages/datatables/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Presensi</li>
    </ol>
@endsection
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <form action="{{ route('presensi.index') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="label-control">Periode Tanggal</label>
                    <div class="row no-gutters">
                        <div class="col-sm-2">
                            <input style="height: 32px" type="text" id="periode" name="periode" class="form-control datepicker" value="{{ $periode }}"/>
                        </div>
                        <div class="col-sm">
                            <input type="submit" name="Tampilkan" class="btn btn-sm btn-outline-primary" value="Tampilkan">
                        </div>
                    </div>
                </div>
            </form>
            <h4 class="card-title ">Riwayat Presensi</h4>
            @if(auth()->user()->getOriginal('role')=='karyawan')
                <form action="{{ route('presensi.store') }}" method="POST">
                    @csrf
                    @if (!auth()->user()->isClockIn())
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#presensi-clockin">Clock-in</button>
                        {{-- <input type="submit" class="btn btn-primary" name="submit" value="Clock-in"> --}}
                    @elseif(!auth()->user()->isClockOut())
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#presensi-clockout">Clock-out</button>
                        {{-- <input type="submit" class="btn btn-warning" name="submit" value="Clock-out"> --}}
                    @else
                        <h4>Anda sudah presensi pulang</h4>
                    @endif
                </form>
            @endif
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table" id="table-presensi">
                <thead class=" text-primary">
                    @if(auth()->user()->getOriginal('role')=='admin')
                        <th>Karyawan</th>
                    @endif
                    <th>Periode</th>
                    <th>Masuk</th>
                    <th>Keluar</th>
                </thead>
                <tbody>
                    @foreach ($presences as $presensi)
                        <tr>
                            @if(auth()->user()->getOriginal('role')=='admin')
                                <td>{{ $presensi->user->name }}</td>
                            @endif
                            <td>{{ Chantika::toLongDate($presensi->periode) }}</td>
                            <td>{{ Chantika::toTime($presensi->clockin) }}</td>
                            <td>{{ ($presensi->clockout) ? Chantika::toTime($presensi->clockout): '-'  }}</td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    <x-modal id="presensi-clockin" title="Presensi Masuk">
        <form action="{{ route('presensi.store') }}" method="post">
            {{ csrf_field() }}
            <h4>Apakah anda yakin ingin presensi masuk?</h4>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-success" value="Clock-in" />
            </div>
        </form>
    </x-modal>
    <x-modal id="presensi-clockout" title="Presensi Keluar">
        <form action="{{ route('presensi.store') }}" method="post">
            {{ csrf_field() }}
            <h4>Apakah anda yakin ingin presensi keluar?</h4>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-warning" value="Clock-out" />
            </div>
        </form>
    </x-modal>
@endsection
@section('js')
    <script src="{{ asset('packages/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('packages/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/moment.locale.id.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $('.datepicker').datetimepicker({
            locale: 'id',
            format: 'MM-YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    $(document).ready(function() {
        $('#table-presensi').DataTable()
    })
    </script>
@endsection

