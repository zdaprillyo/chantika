@extends('layouts.main',['title'=>'Kompensasi'])
@section('css')
    <link rel="stylesheet" href="{{ asset('packages/datatables/dataTables.bootstrap4.min.css') }}">
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Kompensasi</li>
    </ol>
@endsection
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title ">Daftar Kompensasi</h4>
            <button class="btn btn-primary" data-toggle="modal" data-target="#tambah-kompensasi">Tambah</button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table" id="table-kompensasi">
                <thead class=" text-primary">
                    <th>Nama</th>
                    <th>Nominal</th>
                    <th>Jenis</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    @forelse ($compens as $compen)
                        <tr>
                            <td>{{ $compen->nama }}</td>
                            <td class="text-right">{{ Chantika::toRupiah($compen->nominal) }}</td>
                            <td>{{ $compen->jenis }}</td>
                            <td>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#ubah-kompensasi" onclick="ubah({{ $compen->id }})">Ubah</button>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#hapus-kompensasi" onclick="hapus({{ $compen->id }})">Hapus</button>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    <x-modal id="tambah-kompensasi" title="Tambah Kompensasi">
        <form action="{{ route('kompensasi.store') }}" method="post">
            @csrf
            <x-input label="Nama" type="text" name="tambah_nama" placeholder="Masukkan nama kompensasi" required="true"/>
            <x-input label="Nominal" type="text" name="tambah_nominal" placeholder="Masukkan nominal kompensasi" required="true"/>
            <x-select-kompensasi id="tambah_jenis" name="tambah_jenis" />
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-success" value="Simpan" />
            </div>
        </form>
    </x-modal>
    <x-modal id="ubah-kompensasi" title="Ubah Kompensasi">
        <form action="{{ route('kompensasi.update',1) }}" id="form_ubah" method="post">
            @csrf
            @method('PUT')
            <x-input label="Nama" type="text" name="ubah_nama" id="ubah_nama" required="true"/>
            <x-input label="Nominal" type="text" name="ubah_nominal" id="ubah_nominal" required="true"/>
            <x-select-kompensasi id="ubah_jenis" name="ubah_jenis" />
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-success" value="Ubah"/>
            </div>
        </form>
    </x-modal>
    <x-modal id="hapus-kompensasi" title="Hapus Kompensasi">
        <form action="" method="post" id="form_hapus">
            @csrf
            @method('delete')
            <h5>Anda yakin akan menghapus kompensasi ini?</h5>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-danger" value="Hapus" />
            </div>
        </form>
    </x-modal>
@endsection
@section('js')
    <script src="{{ asset('packages/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('packages/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    $(document).ready(function() {
        $('#table-kompensasi').DataTable()
    })

    function ubah(id){
        var urlUpdate = "{{ route('kompensasi.update', "-id-") }}".replace('-id-', id)
        $('#form_ubah').prop('action',urlUpdate)
        let urlShow="{{ route('kompensasi.show', ['kompensasi'=>'-id-']) }}".replace('-id-', id)
        $.ajax({
            type: "GET",
            url: urlShow,
            cache: false,
            success: function(result){
                $("#ubah_nama").val(result.nama)
                $("#ubah_nominal").val(result.nominal)
                $("#ubah_jenis").val(result.jenis)
            },
            error: function (request, status, error) {
                console.log(error)
            }
        })
    }
    function hapus(id){
        var urlUpdate = "{{ route('kompensasi.destroy', "-id-") }}".replace('-id-', id)
        $('#form_hapus').prop('action',urlUpdate)
    }
    </script>
@endsection

