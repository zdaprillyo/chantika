<div class="form-group">
    <label for="select2SinglePlaceholder">Jenis Kompensasi <span class="text-danger">*</span></label>
    <select class="select2-single-placeholder form-control" id="{{ $id }}" name="{{ $name }}">
      <option value="">Pilih jenis kompensasi</option>
      <option value="gaji">Gaji Pokok</option>
      <option value="tunjangan">Tunjangan</option>
      <option value="denda">Denda</option>
    </select>
</div>
