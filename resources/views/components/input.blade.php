{{-- @props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50']) !!}> --}}
<div class="@if ($type == 'hidden') d-none @else form-group @endif">
    @if($label !== "")
        <label class="mb-2 form-control-label" for="{{ $id }}">{{ $label }} @if($required == 'true') <span class="text-danger">*</span> @endif</label>
    @endif
    <input
        type="{{ $type }}" class="form-control {{ $class }} @error('{{ $name }}') is-invalid @enderror"
        name="{{ $name }}" id="{{ $id }}"
        @if ($type == "number")
            min="{{ $numberMin }}"
            max="{{ $numberMax }}"
            step="{{ $numberStep }}"
        @endif
        aria-describedby="{{ $name }}Help"
        placeholder="{{ $placeholder }}"
        @if($type != 'password') value="{{ old($name) ?? $value }}" @endif
        @if($required == 'true' && $requiredOnBack == 'false') required @endif
        @if($disabled == 'true') disabled @endif
        @if($readonly == 'true') readonly @endif
    />
    @error($name)
        <small id="{{ $id }}Help" class="form-text text-danger">{{ $message }}</small>
    @else
        <small id="{{ $id }}Help" class="form-text text-danger"></small>
    @enderror
</div>
