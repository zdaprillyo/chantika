  <div class="form-group">
    <label for="select2SinglePlaceholder">Bulan</label>
    <select class="select2-single-placeholder form-control" id="{{ $id }}" name="{{ $name }}">
        <option value="">Pilih bulan</option>
        <option value="1" @if ($value) selected @endif>Januari</option>
        <option value="2" @if ($value) selected @endif>Februari</option>
        <option value="3" @if ($value) selected @endif>Maret</option>
        <option value="4" @if ($value) selected @endif>April</option>
        <option value="5" @if ($value) selected @endif>Mei</option>
        <option value="6" @if ($value) selected @endif>Juni</option>
        <option value="7" @if ($value) selected @endif>Juli</option>
        <option value="8" @if ($value) selected @endif>Agustus</option>
        <option value="9" @if ($value) selected @endif>September</option>
        <option value="10" @if ($value) selected @endif>Oktober</option>
        <option value="11" @if ($value) selected @endif>November</option>
        <option value="12" @if ($value) selected @endif>Desember</option>
    </select>
  </div>
