<div class="form-group">
    <label for="select2SinglePlaceholder">Tahun</label>
    <select class="select2-single-placeholder form-control" id="{{ $id }}" name="{{ $name }}">
        <option value="">Pilih tahun</option>
        <option value="2018" @if ($value) selected @endif>2018</option>
        <option value="2019" @if ($value) selected @endif>2019</option>
        <option value="2020" @if ($value) selected @endif>2020</option>
        <option value="2021" @if ($value) selected @endif>2021</option>
        <option value="2022" @if ($value) selected @endif>2022</option>
    </select>
  </div>
