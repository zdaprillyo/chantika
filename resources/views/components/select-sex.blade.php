<div class="form-group">
    <label for="select2SinglePlaceholder">Jenis Kelamin <span class="text-danger">*</span></label>
    <select class="select2-single-placeholder form-control" id="{{ $id }}" name="{{ $name }}" required>
      <option value="">Pilih jenis kelamin</option>
      <option value="laki-laki">Laki-laki</option>
      <option value="perempuan">Perempuan</option>
    </select>
</div>
