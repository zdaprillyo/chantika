<div class="form-group text-left">
    @if($label !== "")
        <label class="mb-2 form-control-label" for="{{ $id }}">{{ $label }} @if($required == 'true') <span class="text-danger">*</span> @endif</label>
    @endif
    <select
        name="{{ $name }}" id="{{ $id }}"
        class="form-control @error('{{ $name }}') is-invalid @enderror select2" style="width:100%"
        aria-describedby="{{ $name }}Help"
        @if($required == 'true' && $requiredOnBack == 'false') required @endif
        @if($disabled == 'true') disabled @endif
        @if($multiple == 'true') multiple data-placeholder="{{ $selectedNone }}" @endif
    >
        <option @if($multiple == 'false') selected @endif value="">{{ $selectedNone }}</option>
        @if ($fromModel == 'true')
            @if ($multiple == 'true')
                @php
                    $name = str_replace('[]', '', $name);
                @endphp
                @foreach ($options as $option)
                    @if($value !== "")
                        <option value="{{ $option[$idModel] }}" {{ ($value == $option[$idModel]) ? "selected" : '' }}>{{ $option[$valueModel] }}</option>
                    @else
                        <option value="{{ $option[$idModel] }}" {{ in_array($option[$idModel], (old($name) ?? [])) ? "selected" : '' }}>{{ $option[$valueModel] }}</option>
                    @endif
                @endforeach
            @else
                @foreach ($options as $option)
                    @if($value !== "")
                        <option value="{{ $option[$idModel] }}" {{ ($value == $option[$idModel]) ? "selected" : '' }}>{{ $option[$valueModel] }}</option>
                    @else
                        <option value="{{ $option[$idModel] }}" {{ (old($name) == $option[$idModel]) ? "selected" : '' }}>{{ $option[$valueModel] }}</option>
                    @endif
                @endforeach
            @endif
        @else
            @foreach ($options as $key => $option)
                @if ($multiple == 'true')
                    @php
                        $name = str_replace('[]', '', $name);
                    @endphp
                    @if($value !== "")
                        <option value="{{ $key }}" {{ ($value == $key) ? "selected" : '' }}>{{ $option }}</option>
                    @else
                        <option value="{{ $key }}" {{ in_array($key, (old($name) ?? [])) ? "selected" : '' }}>{{ $option }}</option>
                    @endif
                @else
                    @if($value !== "")
                        <option value="{{ $key }}" {{ ($value == $key) ? "selected" : '' }}>{{ $option }}</option>
                    @else
                        <option value="{{ $key }}" {{ (old($name) == $key) ? "selected" : '' }}>{{ $option }}</option>
                    @endif
                @endif
            @endforeach
        @endif
    </select>
    @error($name)
        <small id="{{ $name }}Help" class="form-text text-danger">{{ $message }}</small>
    @enderror
</div>
