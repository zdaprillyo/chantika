<div class="row no-gutters">
    <div class="col-2">
        <x-select-month id="{{ $idMonth }}" name="{{ $nameMonth }}" value="{{ $valueMonth }}"/>
    </div>
    <div class="col-2">
        <x-select-year id="{{ $idYear }}" name="{{ $nameYear }}" value="{{ $valueYear }}" />
    </div>
</div>
