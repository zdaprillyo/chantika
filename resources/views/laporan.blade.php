@extends('layouts.main',['title'=>'Laporan'])
@section('css')
    <link rel="stylesheet" href="{{ asset('packages/datatables/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Laporan</li>
    </ol>
@endsection
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <form action="{{ route('laporan.index') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="label-control">Periode Tanggal</label>
                    <div class="row no-gutters">
                        <div class="col-sm-2">
                            <input style="height: 32px" type="text" id="periode" name="periode" class="form-control datepicker" value="{{ $periode }}"/>
                        </div>
                        <div class="col-sm">
                            <input type="submit" name="Tampilkan" class="btn btn-sm btn-outline-primary" value="Tampilkan">
                        </div>
                    </div>
                </div>
            </form>
            {{-- <x-select-month-year idMonth="pilih_bulan" nameMonth="pilih_bulan" valueMonth="{{ $bulan }}" idYear="pilih_tahun" nameYear="pilih_tahun" valueYear="{{ $tahun }}"/> --}}
            <h4 class="card-title ">Daftar Laporan</h4>
            @if(auth()->user()->getOriginal('role')=='admin')
                @if (count($reports)<=0)
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#buat-laporan">Buat Laporan</button>
                @endif
            @endif
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table" id="table-laporan">
                <thead class=" text-primary">
                    <th>Periode</th>
                    <th>Karyawan</th>
                    <th>Presensi</th>
                    <th>Absensi</th>
                    <th>Terlambat</th>
                    <th>Lembur</th>
                </thead>
                <tbody>
                    @forelse ($reports as $laporan)
                        <tr>
                            <td>{{ Chantika::toLongDate($laporan->periode) }}</td>
                            <td>{{ ucfirst($laporan->user->name)  }}</td>
                            <td>{{ $laporan->total_presensi }} kali</td>
                            <td>{{ $laporan->total_absensi }} kali</td>
                            <td>{{ $laporan->total_terlambat }} menit</td>
                            <td>{{ $laporan->total_lembur }} menit</td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    <x-modal id="buat-laporan" title="Buat Laporan Presensi">
        <form action="{{ route('laporan.store') }}" method="post">
            @csrf
            {{ csrf_field() }}
            <h4>Apakah anda yakin ingin membuat laporan presensi?</h4>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-success" value="Buat Laporan" />
            </div>
        </form>
    </x-modal>
@endsection
@section('js')
    <script src="{{ asset('packages/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('packages/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/moment.locale.id.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
    $('.datepicker').datetimepicker({
        locale: 'id',
        format: 'MM-YYYY',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });
    $(document).ready(function() {
        $('#table-laporan').DataTable()
    })
    </script>
@endsection

