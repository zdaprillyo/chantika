<?php

use App\Http\Controllers\CompenController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PresenceController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SalaryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';
Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
Route::group(['middleware' => ['auth']], function () {
    //Route::resource('presensi', PresenceController::class);
    Route::resource('kompensasi', CompenController::class);
    Route::resource('karyawan', EmployeeController::class);
    //Route presensi
    Route::match(array('GET','POST'),'presensi', [PresenceController::class,'index'])->name('presensi.index');
    Route::post('presensi-store', [PresenceController::class,'store'])->name('presensi.store');
    //Route ubah profile
    Route::put('update-profile/{user}', [UserController::class,'update'])->name('update.profile');
    //Route laporan absensi
    Route::match(array('GET','POST'),'laporan', [ReportController::class,'index'])->name('laporan.index');
    Route::post('laporan-store', [ReportController::class,'store'])->name('laporan.store');
    //Route gaji
    Route::match(array('GET','POST'),'gaji', [SalaryController::class,'index'])->name('gaji.index');
    Route::post('gaji-store', [SalaryController::class,'store'])->name('gaji.store');
});

Route::get('/profile', function () {
    $user = auth()->user();
    return view('profile',compact('user'));
})->middleware(['auth'])->name('profile');



