<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new  User();
        $user->name = 'Admin';
        $user->username = 'admin';
        $user->email  = 'admin@chantika.com';
        $user->email_verified_at = now();
        $user->password = Hash::make('1234');
        $user->role = 'admin';
        $user->remember_token = now();
        $user->save();
        User::factory(10)->create();
    }
}
