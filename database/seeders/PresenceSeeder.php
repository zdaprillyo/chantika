<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Presence;
use App\Models\User;
class PresenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tglNow = date('Y-m').'-01';
        $users = User::where('role','karyawan')->get();
        for ($i=0; $i < date('t'); $i++) {
            foreach ($users as $karyawan) {
                $p = new Presence();
                $p->periode = $tglNow;
                $p->clockin = date('Y-m-d 07:00');
                $p->clockout = date('Y-m-d 16:00');
                $p->user_id = $karyawan->id;
                $p->save();
            }
            $tglNow = date('Y-m-d', strtotime($tglNow.' +1 day '));
        }
    }
}
