<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Employee;
class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('role','karyawan')->get();
        foreach ($users as $user) {
            $e = new Employee();
            $e->jenis_kelamin = 'laki-laki';
            $e->alamat = 'alamat';
            $e->telepon = '082112341234';
            $e->user_id = $user->id;
            $e->save();
        }
    }
}
