<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Compen;
class CompenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listKompen=[
            ['nama'=>'Gaji','nominal'=>2500000,'jenis'=>'gaji'],
            ['nama'=>'Absen','nominal'=>10000,'jenis'=>'denda'],
            ['nama'=>'Lembur','nominal'=>25000,'jenis'=>'tunjangan'],
            ['nama'=>'Makan','nominal'=>25000,'jenis'=>'tunjangan'],
            ['nama'=>'Rajin','nominal'=>25000,'jenis'=>'tunjangan'],
            ['nama'=>'Terlambat','nominal'=>2000,'jenis'=>'denda'],
        ];
        foreach ($listKompen as $kompen) {
            $k = new Compen();
            $k->nama = $kompen['nama'];
            $k->nominal = $kompen['nominal'];
            $k->jenis = $kompen['jenis'];
            $k->save();
        }
    }
}
