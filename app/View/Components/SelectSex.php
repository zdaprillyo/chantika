<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectSex extends Component
{
    public $name;
    public $id;
    public $required;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id=null,$name=null,$required=null)
    {
        $this->id=$id;
        $this->name=$name;
        $this->required=$required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $id=$this->id;
        $name=$this->name;
        $required=$this->required;
        return view('components.select-sex',compact('id','name','required'));
    }
}
