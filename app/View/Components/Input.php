<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    public $id;
    public $class;
    public $type;
    public $label;
    public $name;
    public $value;
    public $numberMin;
    public $numberMax;
    public $numberStep;
    public $placeholder;
    public $readonly;
    public $required;
    public $disabled;
    public $requiredOnBack;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id ="", $class = '', $type = "text", $label = "", $name='', $value = "", $numberMin = "", $numberMax = "", $numberStep = "", $placeholder = "", $required = "false", $disabled = "false", $readonly = "false", $requiredOnBack = "false")
    {
        $this->id = $id;
        $this->class = $class;
        $this->type = $type;
        $this->label = $label;
        $this->name = $name;
        $this->value = $value;
        $this->numberMin = $numberMin;
        $this->numberMax = $numberMax;
        $this->numberStep = $numberStep;
        $this->placeholder = $placeholder;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->readonly = $readonly;
        $this->requiredOnBack = $requiredOnBack;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input', [
            'id' => $this->id,
            'class' => $this->class,
            'type' => $this->type,
            'label' => $this->label,
            'name' => $this->name,
            'value' => $this->value,
            'numberMin' => $this->numberMin,
            'numberMax' => $this->numberMax,
            'numberStep' => $this->numberStep,
            'placeholder' => $this->placeholder,
            'required' => $this->required,
            'disabled' => $this->disabled,
            'readonly' => $this->readonly,
            'requiredOnBack' => $this->requiredOnBack,
        ]);
    }
}
