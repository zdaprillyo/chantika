<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectYear extends Component
{
    public $name;
    public $id;
    public $value;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id=null,$name=null,$value=null)
    {
        $this->id=$id;
        $this->name=$name;
        $this->value=$value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $id=$this->id;
        $name=$this->name;
        $value=$this->value;
        return view('components.select-year',compact('id','name','value'));
    }
}
