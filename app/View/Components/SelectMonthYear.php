<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectMonthYear extends Component
{
    public $idMonth;
    public $nameMonth;
    public $valueMonth;
    public $idYear;
    public $nameYear;
    public $valueYear;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($idMonth=null,$nameMonth=null,$valueMonth=null,$idYear=null,$nameYear=null,$valueYear=null)
    {
        $this->idMonth=$idMonth;
        $this->nameMonth=$nameMonth;
        $this->valueMonth=$valueMonth;
        $this->idYear=$idYear;
        $this->nameYear=$nameYear;
        $this->valueYear=$valueYear;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $idMonth=$this->idMonth;
        $nameMonth=$this->nameMonth;
        $valueMonth=$this->valueMonth;
        $idYear=$this->idYear;
        $nameYear=$this->nameYear;
        $valueYear=$this->valueYear;
        return view('components.select-month-year',compact('idMonth','nameMonth','valueMonth','idYear','nameYear','valueYear'));
    }
}
