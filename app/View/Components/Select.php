<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Select extends Component
{
    public $id;
    public $label;
    public $name;
    public $options;
    public $value;
    public $selectedNone;
    public $fromModel;
    public $idModel;
    public $valueModel;
    public $required;
    public $requiredOnBack;
    public $disabled;
    public $multiple;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id = '', $label = "", $name='', $options='', $value = "", $selectedNone = "-- Pilih --", $fromModel = "false", $idModel = "", $valueModel = "", $required = "false", $disabled = "false", $multiple = "false", $requiredOnBack = "false")
    {
        $this->id = $id;
        $this->label = $label;
        $this->name = $name;
        $this->options = $options;
        $this->value = $value;
        $this->selectedNone = $selectedNone;
        $this->fromModel = $fromModel;
        $this->idModel = $idModel;
        $this->valueModel = $valueModel;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->multiple = $multiple;
        $this->requiredOnBack = $requiredOnBack;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data = [
            'id' => $this->id,
            'label' => $this->label,
            'name' => $this->name,
            'options' => $this->options,
            'selectedNone' => $this->selectedNone,
            'fromModel' => $this->fromModel,
            'idModel' => $this->idModel,
            'valueModel' => $this->valueModel,
            'required' => $this->required,
            'disabled' => $this->disabled,
            'multiple' => $this->multiple,
            'requiredOnBack' => $this->requiredOnBack,
            'value' => $this->value,
        ];

        return view('components.select', $data);
    }
}
