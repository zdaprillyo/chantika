<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function presences()
    {
        return $this->hasMany(Presence::class);
    }
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
    public function employee()
    {
        return $this->hasOne(Employee::class);
    }
    public function isClockIn(){
        $p = auth()->user()->presences()->whereDate('periode',date('Y-m-d'))->get();
        if(!empty($p[0])){
            return true;
        }else{
            return false;
        }
    }
    public function isClockOut(){
        $p = auth()->user()->presences()->whereDate('periode',date('Y-m-d'))->get();
        if(!empty($p[0])){
            if($p[0]->clockout){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
