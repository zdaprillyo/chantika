<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['periode','gaji_pokok','tunjangan','denda','gaji_bersih','report_id'];
    use HasFactory;
    public function report()
    {
        return $this->belongsTo(Report::class);
    }
    public function compens()
    {
        return $this->belongsToMany(Compen::class)->withPivot(['nominal'])->withTimestamps();
    }
}
