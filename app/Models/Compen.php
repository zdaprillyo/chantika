<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compen extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama','nominal','jenis'];
    use HasFactory;
    public function salaries()
    {
        return $this->belongsToMany(Salary::class)->withPivot(['nominal'])->withTimestamps();
    }
}
