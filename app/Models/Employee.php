<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['jenis_kelamin','alamat','telepon','user_id'];
    use HasFactory;
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
