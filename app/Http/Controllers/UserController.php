<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
class UserController extends Controller
{
    public $fitur="Profile";
    public function update(Request $request,User $user){
        try {
            DB::beginTransaction();
            $user->update([
                'name' => $request->ubah_nama,
                'email' => $request->ubah_email,
            ]);
            $employee = Employee::where('user_id',$user->id)->first();
            $employee->update([
                'jenis_kelamin'=> $request->ubah_jenis_kelamin,
                'alamat' => $request->ubah_alamat,
                'telepon' => $request->ubah_telepon
            ]);
            DB::commit();
            toast()->success("Ubah $this->fitur Berhasil!");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            dd($e->getMassage());
            toast()->error("Ubah $this->fitur Gagal!".$e->getMessage());
            return redirect()->back()->withInput();
        }

        return view('profile');
    }
}
