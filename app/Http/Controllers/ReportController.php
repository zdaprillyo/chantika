<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
class ReportController extends Controller
{
    public $fitur="Laporan Presensi";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bulan = date('m');
        $tahun = date('Y');
        if(!empty($request->all())){
            $bulan = explode('-',$request->periode)[0];
            $tahun = explode('-',$request->periode)[1];
        }
        $periode = "$bulan-$tahun";
        if(auth()->user()->role=='admin'){
            $reports = Report::whereMonth('periode','=',$bulan)->whereYear('periode','=',$tahun)->get();
        }else{
            $reports = Report::where('user_id',auth()->user()->id)->whereMonth('periode','=',$bulan)->whereYear('periode','=',$tahun)->get();
        }
        return view('laporan',compact('reports','periode'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $employees=User::where('role','karyawan')->get();
            foreach ($employees as $karyawan) {
               $tglNow = date('Y-m') . '-01';
               $total_presensi=0;
               $total_absensi=0;
               $total_lembur=0;
               $total_terlambat=0;
               for ($i=0; $i < date('t'); $i++) {
                    $presensi=$karyawan->presences()->whereDate('periode',$tglNow)->get();
                    if(isset($presensi[0])){
                        $total_presensi++;
                        $terlambat = (strtotime(date('H:s:i',strtotime($presensi[0]->clockin)))-strtotime('07:00:00'))/60;
                        ($terlambat>0) ? $total_terlambat += $terlambat : '';
                        $lembur = (strtotime(date('H:s:i',strtotime($presensi[0]->clockout)))-strtotime('16:00:00'))/60;
                        ($lembur>0) ? $total_lembur += $lembur : '';
                    }else{
                        $total_absensi++;
                    }
                    $tglNow = date('Y-m-d', strtotime($tglNow.' +1 day '));
               }
               $laporan = Report::create([
                'periode' => date('Y-m') . '-01',
                'total_terlambat' => $total_terlambat,
                'total_lembur' => $total_lembur,
                'total_presensi' => $total_presensi,
                'total_absensi' => $total_absensi,
                'user_id' => $karyawan->id
               ]);
            }
            DB::commit();
            toast()->success("Tambah $this->fitur Berhasil!");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            toast()->error("Ubah $this->fitur Gagal!. ".$e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }
}
