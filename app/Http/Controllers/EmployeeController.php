<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Validator;
use Exception;
use Hash;
class EmployeeController extends Controller
{
    public $fitur="Karyawan";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role','karyawan')->get();
        return view('karyawan',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $user=user::create([
                'name' => $request->tambah_nama,
                'username' => str_replace(' ', '',strtolower($request->tambah_nama)),
                'email' => $request->tambah_email,
                'password' => Hash::make(strtolower($request->tambah_nama))
            ]);
            $karyawan=Employee::create([
                'jenis_kelamin' => $request->tambah_jenis_kelamin,
                'alamat' => $request->tambah_alamat,
                'telepon' => $request->tambah_telepon,
                'user_id' => $user->id
            ]);
            DB::commit();
            toast()->success("Tambah $this->fitur Berhasil!");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            toast()->error("Tambah $this->fitur Gagal!. ".$e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $karyawan)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $karyawan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $karyawan)
    {
        echo 'update';
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $karyawan)
    {
        //
    }
}
