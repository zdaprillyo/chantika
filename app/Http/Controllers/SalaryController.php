<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Salary;
use App\Models\Compen;
use Illuminate\Http\Request;
use DB;
use Log;
class SalaryController extends Controller
{
    public $fitur="Perhitungan Gaji";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bulan = date('m');
        $tahun = date('Y');
        if(!empty($request->all())){
            $bulan = explode('-',$request->periode)[0];
            $tahun = explode('-',$request->periode)[1];
        }
        $periode = "$bulan-$tahun";
        if(auth()->user()->role=='admin'){
            $salaries = Salary::whereMonth('periode','=',$bulan)->whereYear('periode','=',$tahun)->get();
        }else{
            $reports = Report::where('user_id',auth()->user()->id)->get();
            $report_ids=[];
            foreach ($reports as $rep) { $report_ids[]=$rep->id; }
            $salaries = Salary::whereIn('report_id',$report_ids)->whereMonth('periode','=',$bulan)->whereYear('periode','=',$tahun)->get();
        }
        return view('gaji',compact('salaries','periode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $bulan = date('m');
            $tahun = date('Y');
            $reports = Report::whereMonth('periode',$bulan)->whereYear('periode',$tahun)->get();
            //start digunakan untuk mencari data kompensasi
            $kompensasi=[];
            $ntgaji = Compen::where('jenis','gaji')->get()[0];
            $ntmakan = Compen::where('nama','Makan')->get()[0];
            $ntrajin = Compen::where('nama','Rajin')->get()[0];
            $ntterlambat = Compen::where('nama','Terlambat')->get()[0];
            $ntlembur = Compen::where('nama','Lembur')->get()[0];
            $ntabsen = Compen::where('nama','Absen')->get()[0];
            $kompensasi['ntgaji']=$ntgaji;
            $kompensasi['ntmakan']=$ntmakan;
            $kompensasi['ntrajin']=$ntrajin;
            $kompensasi['ntterlambat']=$ntterlambat;
            $kompensasi['ntlembur']=$ntlembur;
            $kompensasi['ntabsen']=$ntabsen;
            // end
            foreach ($reports as $laporan) {
              $tlembur = ($laporan->total_lembur/60)* $kompensasi['ntlembur']->nominal;
              $tmakan = $laporan->total_presensi * $kompensasi['ntmakan']->nominal;
              $dterlambat = $laporan->total_terlambat * $kompensasi['ntterlambat']->nominal;
              $dabsen = $laporan->total_absensi * $kompensasi['ntabsen']->nominal;
              $gaji_bersih = $kompensasi['ntgaji']->nominal + $tlembur + $tmakan - $dabsen - $dterlambat;
              if($laporan->total_absensi==0){
                $gaji_bersih+=$kompensasi['ntrajin']->nominal;
              }
              $gaji = Salary::create([
                  'periode' => $laporan->periode,
                  'gaji_pokok' => $kompensasi['ntgaji']->nominal,
                  'tunjangan' => $tlembur + $tmakan,
                  'denda' => $dabsen + $dterlambat,
                  'gaji_bersih' => $gaji_bersih,
                  'report_id' => $laporan->id,
              ]);
              foreach ($kompensasi as $kompen) {
                $gaji->compens()->attach($kompen->id,['nominal'=>$kompen->nominal]);
              }
            }
            DB::commit();
            toast()->success("Tambah $this->fitur Berhasil!");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            toast()->error("Ubah $this->fitur Gagal!. ".$e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function show(Salary $salary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $salary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salary $salary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salary $salary)
    {
        //
    }
}
