<?php

namespace App\Http\Controllers;

use App\Models\Presence;
use Illuminate\Http\Request;
use DB;
use Validator;
use Exception;
use App\Helpers\Chantika;
class PresenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bulan = date('m');
        $tahun = date('Y');
        if(!empty($request->all())){
            $bulan = explode('-',$request->periode)[0];
            $tahun = explode('-',$request->periode)[1];
        }
        $periode = "$bulan-$tahun";

        if(auth()->user()->role=='admin'){
            $presences = Presence::whereMonth('periode','=',$bulan)->whereYear('periode','=',$tahun)->get();
        }else{
            $presences = Presence::where('user_id',auth()->user()->id)->whereMonth('periode','=',$bulan)->whereYear('periode','=',$tahun)->get();
        }
        return view('presensi',compact('presences','periode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if(!auth()->user()->isClockIn()){
                $presensi=Presence::create([
                    'periode' => date('Y-m-d'),
                    'clockin' => now(),
                    'user_id' => auth()->user()->id,
                ]);
            }else {
                $presensi=Presence::where('user_id',auth()->user()->id)->whereDate('periode',date('Y-m-d'))->get();
                $presensi[0]->update(['clockout'=>now()]);
            }
            DB::commit();
            toast()->success('Presensi Berhasil!');
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            toast()->error('Presensi Gagal!. '.$e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Presence  $presence
     * @return \Illuminate\Http\Response
     */
    public function show(Presence $presence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Presence  $presence
     * @return \Illuminate\Http\Response
     */
    public function edit(Presence $presence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Presence  $presence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Presence $presence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Presence  $presence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Presence $presence)
    {
        //
    }
}
