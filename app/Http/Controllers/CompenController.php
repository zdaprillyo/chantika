<?php

namespace App\Http\Controllers;

use App\Models\Compen;
use Illuminate\Http\Request;
use DB;
class CompenController extends Controller
{

    public $fitur="Kompensasi";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compens=Compen::all();
        return view('kompensasi',compact('compens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        try {
            DB::beginTransaction();
            $kompen=Compen::create([
                'nama' => $request->tambah_nama ,
                'nominal' => $request->tambah_nominal ,
                'jenis' => $request->tambah_jenis,
            ]);
            DB::commit();
            toast()->success("Tambah $this->fitur Berhasil!");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            dd($e->getMassage());
            toast()->error("Tambah $this->fitur Gagal!. ".$e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Compen  $kompensasi
     * @return \Illuminate\Http\Response
     */
    public function show(Compen $kompensasi,Request $request)
    {
       return response()->json($kompensasi);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Compen  $kompensasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Compen $kompensasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Compen  $kompensasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compen $kompensasi)
    {
       // dd($request->all());
        try {
            DB::beginTransaction();
            $kompensasi->update([
                'nama' => $request->ubah_nama ,
                'nominal' => $request->ubah_nominal ,
                'jenis' => $request->ubah_jenis,
            ]);
            DB::commit();
            toast()->success("Ubah $this->fitur Berhasil!");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            dd($e->getMassage());
            toast()->error("Ubah $this->fitur Gagal!".$e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Compen  $kompensasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compen $kompensasi)
    {
        try {
            DB::beginTransaction();
            $nama = $kompensasi->nama;
            $kompensasi->delete();
            DB::commit();
            toast()->success("Hapus $this->fitur $nama Berhasil!");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            dd($e->getMassage());
            toast()->error("Ubah $this->fitur Gagal!".$e->getMessage());
            return redirect()->back()->withInput();
        }
    }
}
