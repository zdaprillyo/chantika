<?php
namespace App\Helpers;

class Chantika{

	public static function toLongDate($date)
	{
		$splited = str_split(date('dmy', strtotime($date)), 2);

		$day = $splited[0];
		$month = self::longMonth(intval($splited[1]));
		$year = date('Y', strtotime($date));

		return "$day $month $year";
	}
    public static function longMonth($month)
    {
        $longMonth = [
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember'
        ];

        return $longMonth[$month];
    }
    public static function toTime($date){
        return date('H:i:s',strtotime($date));
    }
    public static function toRupiah($nominal)
	{
		return number_format($nominal,0,',','.');
    }
}
?>
